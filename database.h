/*
 * Database module header file.
 *
 */
 
#ifndef DATABASE_H
#define DATABASE_H

#include "global.h"

#define NUMBER_OF_LINES 2
#define NAME_LENGTH 20


// A bus stop consists of name and position.
typedef struct Stop Stop;
struct Stop {
	char name[NAME_LENGTH];
	Position position;
};


// Line consists of 2 routes, which are arrays of bus 
// stops to different directions.
typedef struct {
	char name[NAME_LENGTH];
	Stop* routes[2];
	char routeName[2][NAME_LENGTH];
	short routeLength[2];
} Line;



void buildDatabase(void);

extern Line* allLines[NUMBER_OF_LINES];
extern Line* currentLine;
extern int currentRoute;

#endif
