/* 
 * Display stop module.
 *
 */
#include <stdio.h>
#include <string.h>

#include "FreeRTOS.h"
#include "task.h"

#include "setup.h"
#include "assert.h"
#include "stm3210c_eval_ioe.h"

#include "GLCD.h"
#include "display.h"


//********************* Create any line given two points ***********************/
void DrawAnyLine(int x0,int y0,int x1,int y1)
{
  int dx = abs(x1-x0), sx = x0<x1 ? 1 : -1;
  int dy = abs(y1-y0), sy = y0<y1 ? 1 : -1; 
  int err = (dx>dy ? dx : -dy)/2, e2;
 
  for(;;){
    GLCD_putPixel(x0,y0);
    if (x0==x1 && y0==y1) break;
    e2 = err;
    if (e2 >-dx) { err -= dy; x0 += sx; }
    if (e2 < dy) { err += dx; y0 += sy; }
  }
}
/********************************************************/


/******* Create Filled Circle given the coordinates of the center and the radius *********/
void DrawFilledCircle(int x0, int y0, int radius)
{
  int x = radius, y = 0;
  int radiusError = 1-x;
 
  while(x >= y)
  {
		DrawAnyLine(-x+x0,y+y0,x+x0,y+y0);
		DrawAnyLine(-y+x0,x+y0,y+x0,x+y0);
		DrawAnyLine(-x+x0,-y+y0,x+x0,-y+y0);
		DrawAnyLine(-y+x0,-x+y0,y+x0,-x+y0);
		
    y++;
    if (radiusError<0)
    {
      radiusError += 2 * y + 1;
    } else {
      x--;
      radiusError+= 2 * (y - x + 1);
    }
  }
}
/********************************************************/


void Create_Bus_button(int x, int y){

	//back-ground color
	GLCD_clearLn(Line0);
	GLCD_clearLn(Line1);
	GLCD_clearLn(Line2);
	GLCD_clearLn(Line3);
	GLCD_clearLn(Line4);
	GLCD_setTextColor(Blue);
  GLCD_displayStringLn(Line1, (unsigned char*)" WELCOME");

	
	GLCD_setTextColor(White);
	//bus color
	GLCD_setTextColor(Green );
	//bus icon measurement
	GLCD_fillRect(x-12, y-13, 60, 130);
	GLCD_setTextColor(Blue );
	GLCD_fillRect(x-2, y-(-4), 20, 60);
		GLCD_setTextColor(Blue );
	GLCD_fillRect(x-2, y+77, 20, 40);
  //bus tyre color
	GLCD_setTextColor(Black);
	//bud tyre measurement
	DrawFilledCircle(x+50, y+95, 10);
  DrawFilledCircle(x+50, y+8, 10);
}
/********************************************************/

