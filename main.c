/**
 * Program skeleton for the course "Programming embedded systems"
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>

/* Scheduler includes. */
#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"
#include "queue.h"
#include "setup.h"
#include "assert.h"
#include "GLCD.h"
#include "stm3210c_eval_ioe.h"


/* Hardware includes. */
#include "stm32f10x_rcc.h"
#include <stm32f10x_gpio.h>
#include <stm32f10x_exti.h>
#include <misc.h>


/* Application includes. */
#include "nmea_parser.h"
#include "location_types.h"
#include "nextstop.h"
#include "global.h"
#include "database.h"
#include "display.h"
/*-----------------------------------------------------------*/

// Display resources
#define WIDTH 320
xSemaphoreHandle lcdLock;

/* USART resources */
char buf1[150];
xQueueHandle rxq;
xSemaphoreHandle xButtonSemaphore;

/* NMEA Parser resources */
struct gps_position gps_data;		//Relevant data used by system
struct messages nmea_messages;	//Temporary space to store parsed messages, used by parse_message()
xSemaphoreHandle gps_data_sem;
xQueueHandle printQueue;
xQueueHandle skipStopQueue;

xTaskHandle xHandleStart;
xTaskHandle xHandleSim;


/*-------------------------------------------------------------*/

// Converts GPS coordinate to decimal degree format.
// (DDDMM.MMMM -> DD.DDDD)
double toDecimalDegree(double coordinate)
{
	double degree = floor(coordinate/100.0);
	double minutes = coordinate - 100.0*degree;
	return degree + minutes/60.0;
}

//GUI screen identifier
typedef enum {
	SCREEN_ZERO, SCREEN_ONE, SCREEN_TWO, SCREEN_THREE	
} screen;

//GUI screen struct
typedef struct {
	uint16_t routeSelection;
	uint16_t lineSelection;
	uint8_t selected;
	uint8_t stannar;
	screen menuScreen;
	uint8_t screenTouched;
} Selection;


Selection MenuSelection;
/* USART resources */
char buf1[150];
xQueueHandle rxq;
/* NMEA Parser resources */
struct gps_position gps_data;		//Relevant data used by system
struct messages nmea_messages;	//Temporary space to store parsed messages, used by parse_message()
xSemaphoreHandle gps_data_sem;

/* ISR for USART, reads a char and enqueues it */
void uartHandler() {
		portBASE_TYPE xHigherPriorityTaskWokenRCV; 
		//portBASE_TYPE xHigherPriorityTaskWokenSEND;
		portBASE_TYPE res;
		char temp = 0;
		/* Is it time for vATask() to run? */
		xHigherPriorityTaskWokenRCV = pdFALSE;
		if(USART_GetITStatus(USART2, USART_IT_RXNE) != RESET)
		{
      temp = USART_ReceiveData(USART2);
			res = xQueueSendToBackFromISR(rxq, &temp, &xHigherPriorityTaskWokenRCV);
			if(res == errQUEUE_FULL) {
				//Queue is full, we'll miss a char here
				//can't be allowed to happen
				res = 0;
			}
    }
		
		if(USART_GetITStatus(USART2, USART_IT_TXE) != RESET)
		{   
				//not sending any data yet
		}		
		
		/* If xHigherPriorityTaskWoken was set to true you
		we should yield.  The actual macro used here is 
		port specific. */
		if(xHigherPriorityTaskWokenRCV) {
			portEND_SWITCHING_ISR(xHigherPriorityTaskWokenRCV);
		}
}



Position getCurrentPosition(void)
{
	Position pos;
	
	xSemaphoreTake(gps_data_sem, portMAX_DELAY);
	pos.lat = gps_data.latitude;
	pos.lon = gps_data.longitude;
	xSemaphoreGive(gps_data_sem);

	return pos;
}


static void initDisplay () {
  /* LCD Module init */
  lcdLock = xSemaphoreCreateMutex();
  GLCD_init();
  GLCD_clear(White);
}

//GUI screen Task
static void lcdTask(void *params) {

	static int i;
	static uint16_t a = 0;
	uint16_t previousSelection = 0;
	Stop* shownStop = NULL;
	portTickType lastWakeTime = xTaskGetTickCount();
	Stop* stannarStop = NULL;
	

	char sat[20];
	char lat[20];
	char lon[20];
	char tim[20];
  for (;;) {
		int printhours;
		int printseconds;

		
		xSemaphoreTake(gps_data_sem, portMAX_DELAY);
		
		printhours = (gps_data.time.hours+2)%24; // swedish time zone and daylight saving = UTC+2
		printseconds = (int)gps_data.time.seconds;

		sprintf(sat, "Satellites: %d ",gps_data.satellites_used);
		sprintf(lat, "Lat: %4.7f ", gps_data.latitude);
		sprintf(lon, "Lon: %4.7f ", gps_data.longitude);
		sprintf(tim, "Time: %02d:%02d:%02d", printhours, gps_data.time.minutes, printseconds);

		xSemaphoreGive(gps_data_sem);
		
		xSemaphoreTake(lcdLock, portMAX_DELAY);
		//GUI Initial screen
		if(MenuSelection.menuScreen == SCREEN_ZERO){
			GLCD_clearLn(Line4);
			GLCD_clearLn(Line5);
			GLCD_clearLn(Line6);
			GLCD_clearLn(Line7);
			
			GLCD_displayStringLn(Line4, (unsigned char*)" Please Select Line");
		
			MenuSelection.menuScreen = SCREEN_ONE;
		
		}
		//GUI screen One
		if(MenuSelection.menuScreen == SCREEN_ONE){
			
			xSemaphoreTake(xButtonSemaphore, portMAX_DELAY);
			
			if(previousSelection != MenuSelection.lineSelection){
				previousSelection = MenuSelection.lineSelection;
				
				GLCD_setTextColor(Blue);
				GLCD_displayStringLn(Line5, (unsigned char*)allLines[0]->name);
				GLCD_displayStringLn(Line6, (unsigned char*)allLines[1]->name);
			
				//animation for line selection
				GLCD_setTextColor(Red);
				switch(MenuSelection.lineSelection){
					case 1	:	GLCD_displayStringLn(Line5, (unsigned char*)allLines[0]->name);break;
					case 2	: GLCD_displayStringLn(Line6, (unsigned char*)allLines[1]->name);break;
					default	: break;
				}
			}
			
			//handle for after line is selected
			if(MenuSelection.selected){
				GLCD_clearLn(Line4);
				GLCD_clearLn(Line5);
				GLCD_clearLn(Line6);
				GLCD_setTextColor(Olive);
					switch(MenuSelection.lineSelection){
						case 1	:	GLCD_displayStringLn(Line4, (unsigned char*)allLines[0]->name);break;
						case 2	: GLCD_displayStringLn(Line4, (unsigned char*)allLines[1]->name);break;
						default	: break;
					}
				GLCD_displayStringLn(Line5, (unsigned char*)"Selected");
					
				currentLine = allLines[MenuSelection.lineSelection - 1];
				MenuSelection.selected = 0;
				previousSelection = 0;		
				MenuSelection.menuScreen = SCREEN_TWO;
			}
			xSemaphoreGive(xButtonSemaphore);	
		}

		//GUI screen two
		if(MenuSelection.menuScreen == SCREEN_TWO){
		
			xSemaphoreTake(xButtonSemaphore, portMAX_DELAY);
		
			if(previousSelection != MenuSelection.lineSelection){
				previousSelection = MenuSelection.lineSelection;
				
				GLCD_setTextColor(Purple);
				GLCD_displayStringLn(Line5, (unsigned char*)"Please Select Route");
				GLCD_setTextColor(Blue);
				
				GLCD_displayStringLn(Line6, (unsigned char*)currentLine->routeName[0]);
				GLCD_displayStringLn(Line7, (unsigned char*)currentLine->routeName[1]);
		
				//animation for route selection
				GLCD_setTextColor(Red);
				switch(MenuSelection.lineSelection){
					case 1	:	GLCD_displayStringLn(Line6, (unsigned char*)currentLine->routeName[0]);break;
					case 2	: GLCD_displayStringLn(Line7, (unsigned char*)currentLine->routeName[1]);break;
					default	: break;
				}
			}
			
			//handle for after route is selected
			if(MenuSelection.selected){
				GLCD_clear(White);
				GLCD_setTextColor(Olive);
					switch(MenuSelection.lineSelection){
					case 1	:	GLCD_displayStringLn(Line6, (unsigned char*)currentLine->routeName[0]);break;
					case 2	: GLCD_displayStringLn(Line7, (unsigned char*)currentLine->routeName[1]);break;
					default	: break;
					}
				currentRoute = (MenuSelection.lineSelection ) - 1;
				MenuSelection.selected = 0;
				previousSelection = 0;	
				MenuSelection.menuScreen = SCREEN_THREE;
			}
			xSemaphoreGive(xButtonSemaphore);		
		}

		//GUI screen three
		if(MenuSelection.menuScreen == SCREEN_THREE){
			xSemaphoreGive(lcdLock);
			xSemaphoreTake(gps_data_sem, portMAX_DELAY);

			printhours = (gps_data.time.hours+2)%24; // swedish time zone and daylight saving = UTC+2
			printseconds = (int)gps_data.time.seconds;
		
			sprintf(sat, "Satellites: %d",gps_data.satellites_used);
			sprintf(lat, "Lat: %4.7f", gps_data.latitude);
			sprintf(lon, "Lon: %4.7f", gps_data.longitude);
			sprintf(tim, "Time: %02d:%02d:%02d", printhours, gps_data.time.minutes, printseconds);

			xSemaphoreGive(gps_data_sem);
			
			xSemaphoreTake(lcdLock, portMAX_DELAY);
			xQueueReceive(skipStopQueue, &i, (portTickType) 2);

			
			// Show next stop and distance to it.
			if (nextStop != NULL)
			{
				Position current = getCurrentPosition();
				double dist = distanceInMeters(&current, &(nextStop->position));
				char buf[16];
				sprintf(buf, "dist: %.2f ", dist);
				
				GLCD_setTextColor(Black);
				GLCD_clearLn(Line7);
				GLCD_displayStringLn(Line7, (unsigned char*)buf); 
				if (shownStop != nextStop)
				{
					GLCD_clearLn(Line6);
					GLCD_displayStringLn(Line6, (unsigned char*)nextStop->name);
					shownStop = nextStop;
				}
				
				//If key button is pressed 
				if(MenuSelection.stannar){
						GLCD_clearLn(Line5);
						GLCD_displayStringLn(Line5, (unsigned char*)"STANNAR:");
						stannarStop = nextStop;
						MenuSelection.stannar = 0;
				}
				if((stannarStop != nextStop) && (stannarStop != NULL) ){
					GLCD_clearLn(Line5);
					stannarStop = NULL;
					GLCD_clearLn(Line5);
				}				
			}
			else{
				GLCD_clearLn(Line6);
				GLCD_displayStringLn(Line6, (unsigned char*)"(No data)");
			}
			
			//if center of joystick is pressed during screen three, credits will be shown
			if(MenuSelection.selected){
				GLCD_clearLn(Line1);
				GLCD_clearLn(Line2);
				GLCD_clearLn(Line3);
				GLCD_clearLn(Line4);
				GLCD_displayStringLn(Line0, (unsigned char*)"GPS based Next STOP");
				GLCD_displayStringLn(Line1, (unsigned char*)"Display of a Bus");
				GLCD_displayStringLn(Line2, (unsigned char*)"Aditya, Johannes");
				GLCD_displayStringLn(Line3, (unsigned char*)"Lauri, Mohit");
				MenuSelection.selected = 0;
				a = 1;
			}
			//counter for how long the credits will be shown
			if(!MenuSelection.selected && (a > 0))
				a++;
			
			//counter is exceed, back to normal screen information
			if(a > 15){
				GLCD_clearLn(Line0);
				GLCD_clearLn(Line1);
				GLCD_clearLn(Line2);
				GLCD_clearLn(Line3);
				a = 0;
			}
			if(!a){
				GLCD_displayStringLn(Line0, (unsigned char*) sat);
				GLCD_displayStringLn(Line1, (unsigned char*) lat);
				GLCD_displayStringLn(Line2, (unsigned char*) lon);
				GLCD_displayStringLn(Line3, (unsigned char*) tim);	
			}
		}
		xSemaphoreGive(lcdLock);
		
		vTaskDelayUntil(&lastWakeTime, 20 / portTICK_RATE_MS);
  }
}

/*-----------------------------------------------------------*/

/**
 * Display stdout on the display
 */

//printf task to screen from skeleton
static void printTask(void *params) {
  unsigned char str[21] = "                    ";
  portTickType lastWakeTime = xTaskGetTickCount();
  int i;

  for (;;) {
    xSemaphoreTake(lcdLock, portMAX_DELAY);
    GLCD_setTextColor(Black);
    GLCD_displayStringLn(Line9, str);
    xSemaphoreGive(lcdLock);

    for (i = 0; i < 19; ++i)
	  str[i] = str[i+1];

    if (!xQueueReceive(printQueue, str + 19, 0))
	  str[19] = ' ';

	vTaskDelayUntil(&lastWakeTime, 100 / portTICK_RATE_MS);
  }
}

/* Retarget printing to the serial port 1 */
int fputc(int ch, FILE *f) {
  unsigned char c = ch;
  xQueueSend(printQueue, &c, 0);
  return ch;
}
/*-----------------------------------------------------------*/

//interrupt handler for keybutton
void EXTI9_5_IRQHandler(void)
{
	if(EXTI_GetITStatus(EXTI_Line9) != RESET){
		MenuSelection.stannar = 1;
		EXTI_ClearITPendingBit(EXTI_Line9);
	}
}
/*-----------------------------------------------------------*/

//external interrupt initialator for keybutton
void EXTI_Config(void){
  GPIO_InitTypeDef   GPIO_InitStructure;
  EXTI_InitTypeDef   EXTI_InitStructure;
  NVIC_InitTypeDef   NVIC_InitStructure;

/* Enable GPIOB clock */
  RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB, ENABLE);

  /* Configure PB.09 pin as input floating */
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_9;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
  GPIO_Init(GPIOB, &GPIO_InitStructure);

  /* Enable AFIO clock */
  RCC_APB2PeriphClockCmd(RCC_APB2Periph_AFIO, ENABLE);
  /* Connect EXTI9 Line to PB.09 pin */
  GPIO_EXTILineConfig(GPIO_PortSourceGPIOB, GPIO_PinSource9);

  /* Configure EXTI9 line */
  EXTI_InitStructure.EXTI_Line = EXTI_Line9;
  EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
  EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Falling;
  EXTI_InitStructure.EXTI_LineCmd = ENABLE;
  EXTI_Init(&EXTI_InitStructure);

  /* Enable and set EXTI9_5 Interrupt to the lowest priority */
  NVIC_InitStructure.NVIC_IRQChannel = EXTI9_5_IRQn;
  NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0x0F;
  NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0x0F;
  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;

  NVIC_Init(&NVIC_InitStructure);
  


/* Enable GPIOB clock */
  RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB, ENABLE);

  /* Configure PB.09 pin as input floating */
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_9;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
  GPIO_Init(GPIOB, &GPIO_InitStructure);

  /* Enable AFIO clock */
  RCC_APB2PeriphClockCmd(RCC_APB2Periph_AFIO, ENABLE);
  /* Connect EXTI9 Line to PB.09 pin */
  GPIO_EXTILineConfig(GPIO_PortSourceGPIOB, GPIO_PinSource9);

  /* Configure EXTI9 line */
  EXTI_InitStructure.EXTI_Line = EXTI_Line9;
  EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
  EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Falling;
  EXTI_InitStructure.EXTI_LineCmd = ENABLE;
  EXTI_Init(&EXTI_InitStructure);

  /* Enable and set EXTI9_5 Interrupt to the lowest priority */
  NVIC_InitStructure.NVIC_IRQChannel = EXTI9_5_IRQn;
  NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0x0F;
  NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0x0F;
  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;

  NVIC_Init(&NVIC_InitStructure);

}
/*-----------------------------------------------------------*/
//task for handling the input from joystick press
static void menuSelectionTask(void *params) {

	JOY_State_TypeDef pressKey;
	JOY_State_TypeDef sendKey = JOY_NONE;
	portTickType xLastWakeTime;
	static int i = 0;
	xLastWakeTime = xTaskGetTickCount();
	
  for (;;) {
		
		pressKey =  IOE_JoyStickGetState();
		if (pressKey != sendKey){
			sendKey = pressKey;
		}
		else{
			i++;
		}
		
		//make sure the button have been pressed long enough
		if(i == 2){
			xSemaphoreTake(xButtonSemaphore, portMAX_DELAY);
			
			if(sendKey == JOY_DOWN)
				MenuSelection.lineSelection++;
			else if (sendKey == JOY_UP)
				MenuSelection.lineSelection--;
			
			if(MenuSelection.lineSelection > 2)
				MenuSelection.lineSelection = 2;
			else if (MenuSelection.lineSelection < 1)
				MenuSelection.lineSelection = 1;
			
			if(sendKey == JOY_CENTER)
				MenuSelection.selected = 1;
					
			xSemaphoreGive(xButtonSemaphore);
			i = 0;
		}

		vTaskDelayUntil(&xLastWakeTime, 50 / portTICK_RATE_MS);
  }
}
/*-----------------------------------------------------------*/


/* Parses message in buf1 and populates gps_data struct */
void parse_message(void) {
	MSG_ID message_type;
	message_type = parser(buf1, &nmea_messages);
	if(message_type == NMEA_ERROR) {
		return;
	}
	if(message_type == GPGGA) {
		if(nmea_messages.gpgga.latitude == -1 || 
			 nmea_messages.gpgga.longitude == -1 ||
			 nmea_messages.gpgga.ew == 0 ||
			 nmea_messages.gpgga.ns == 0 ||
			 nmea_messages.gpgga.sats_used == -1 ||
			 nmea_messages.gpgga.pos_indicator == 0 ||
			 nmea_messages.gpgga.pos_indicator == -1) {
				 return;
		}
		else {
			xSemaphoreTake(gps_data_sem, portMAX_DELAY);
			gps_data.time.hours = nmea_messages.gpgga.time.hours;
			gps_data.time.minutes = nmea_messages.gpgga.time.minutes;
			gps_data.time.seconds = nmea_messages.gpgga.time.seconds;
			gps_data.longitude = toDecimalDegree(nmea_messages.gpgga.longitude * (nmea_messages.gpgga.ew == 'E' ? 1.0 : -1.0));
			gps_data.latitude = toDecimalDegree(nmea_messages.gpgga.latitude * (nmea_messages.gpgga.ns == 'N' ? 1.0 : -1.0));
			gps_data.satellites_used = nmea_messages.gpgga.sats_used;
			xSemaphoreGive(gps_data_sem);
		}
	}
	if(message_type == GPGLL) {
		if(nmea_messages.gpgll.latitude == -1 ||
			 nmea_messages.gpgll.longitude == -1 ||
			 nmea_messages.gpgll.ew == 0 ||
			 nmea_messages.gpgll.ns == 0 ||
			 nmea_messages.gpgll.status == 'V' ||
			 nmea_messages.gpgll.status == 0) {
				 return;
		}
		xSemaphoreTake(gps_data_sem, portMAX_DELAY);
		gps_data.time.hours = nmea_messages.gpgll.time.hours;
		gps_data.time.minutes = nmea_messages.gpgll.time.minutes;
		gps_data.time.seconds = nmea_messages.gpgll.time.seconds;
		gps_data.longitude = toDecimalDegree(nmea_messages.gpgll.longitude * (nmea_messages.gpgll.ew == 'E' ? 1.0 : -1.0));
		gps_data.latitude = toDecimalDegree(nmea_messages.gpgll.latitude * (nmea_messages.gpgll.ns == 'N' ? 1.0 : -1.0));
		xSemaphoreGive(gps_data_sem);
	}
	if(message_type == GPRMC) {
		if(nmea_messages.gprmc.latitude == -1 ||
			 nmea_messages.gprmc.longitude == -1 ||
			 nmea_messages.gprmc.ew == 0 ||
			 nmea_messages.gprmc.ns == 0 ||
			 nmea_messages.gprmc.status == 'V' ||
			 nmea_messages.gprmc.status == 0) {
				 return;
		}
		xSemaphoreTake(gps_data_sem, portMAX_DELAY);
		gps_data.time.hours = nmea_messages.gprmc.time.hours;
		gps_data.time.minutes = nmea_messages.gprmc.time.minutes;
		gps_data.time.seconds = nmea_messages.gprmc.time.seconds;
		gps_data.longitude = toDecimalDegree(nmea_messages.gprmc.longitude * (nmea_messages.gprmc.ew == 'E' ? 1.0 : -1.0));
		gps_data.latitude = toDecimalDegree(nmea_messages.gprmc.latitude * (nmea_messages.gprmc.ns == 'N' ? 1.0 : -1.0));
		xSemaphoreGive(gps_data_sem);
	}
}


void parseNMEATask(void *params) {
	int i = 0;
	char temp = 0;
	int dollar = 0;
	int cr = 0;
	int lf = 0;
	portBASE_TYPE q_res = pdTRUE;
	NVIC_InitTypeDef NVIC_InitStructure;
	  /* Configure the NVIC Preemption Priority Bits */  
  NVIC_PriorityGroupConfig(NVIC_PriorityGroup_0);
  
  /* Enable the USART2 Interrupt */
  NVIC_InitStructure.NVIC_IRQChannel = USART2_IRQn;
  NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
  NVIC_Init(&NVIC_InitStructure);
	while(1) {
		q_res = xQueueReceive(rxq, &temp, portMAX_DELAY);
		if(q_res == pdFALSE || temp == 0) {continue;}
		
		if(temp == '\r') {
			cr = i;
		}
		if(temp == '\n') {
			lf = i;	
		}
		if(temp == '$') {
				dollar = 1;
		}
		if(dollar){
			buf1[i] = temp;
			i++;
			if(cr != 0 && lf != 0) {
				buf1[cr] = '\0';
				buf1[lf] = 0;
				i = 0;
				cr = 0;
				lf = 0;
				dollar = 0;
				//Whole message received, parse it and populate gps_data struct;
				parse_message();
			}
		}				
	}
}

/*-----------------------------------------------------------*/

/*
 * Entry point of program execution
 */
int main( void )
{
	//struct GUI initial value
	MenuSelection.lineSelection = 1;
	MenuSelection.selected = 0;
	MenuSelection.stannar = 0;
	MenuSelection.menuScreen = SCREEN_ZERO;
	MenuSelection.screenTouched = 0;
	
	// Initialize semaphores
	xButtonSemaphore 	= xSemaphoreCreateMutex();
	gps_data_sem 			= xSemaphoreCreateMutex();
	skipStopQueue 		= xQueueCreate(1, sizeof(int));
		
  prvSetupHardware();
  IOE_Config();

  printQueue = xQueueCreate(128, 1);
	
  initDisplay();
	EXTI_Config();
	
	//Create semaphores
	gps_data_sem = xSemaphoreCreateMutex();
	
	//Create queue for read characters
	rxq = xQueueCreate(128, sizeof(char));
	//Create queue for read characters
	rxq = xQueueCreate(128, sizeof(char));

	xTaskCreate(parseNMEATask, "r_uart", 256, NULL, 3, NULL);
  xTaskCreate(lcdTask, "lcd", 200, NULL, 1, NULL);
  xTaskCreate(printTask, "print", 100, NULL, 1, NULL);
	xTaskCreate(nextStopTask, "distance calculator", 100, NULL, 1, NULL);
	xTaskCreate(menuSelectionTask, (signed char*)"menuSelect", 100, NULL, 1, NULL);

	//for initialize picture
	Create_Bus_button(30,50);

	buildDatabase();
	
  vTaskStartScheduler();

  assert(0);
  return 0;                 // not reachable
}
/*-----------------------------------------------------------*/

