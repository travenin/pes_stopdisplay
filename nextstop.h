/*
 * Next stop module header file.
 *
 */

#ifndef NEXTSTOP_H
#define NEXTSTOP_H

#include "global.h"

// Within this distance to bus stop, the bus is considered to be at it. 
// Threshold value meters.
#define NEAR_THRESHOLD 25.0

// The usual PI/180 constant
static const double DEG_TO_RAD = 0.017453292519943295769236907684886;

// Earth's quatratic mean radius for WGS-84
static const double EARTH_RADIUS_IN_METERS = 6372797.560856;


void nextStopTask(void* params);
Stop* getNextStop(void);
double arcInRadians(Position* from, Position* to);
double distanceInMeters(Position* from, Position* to);


extern Stop* nextStop;
extern xSemaphoreHandle nextStopLock;

#endif
