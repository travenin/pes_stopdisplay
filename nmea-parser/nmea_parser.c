/* Parser for NMEA messages, handles GPGGA, GPGLL, GPGSA, GPGSV, GPRMC, GPVTG */
/* Use parse() to parse a message of specific type, needs just specific message struct
	 Use parser() to handle all six message types, needs a messages struct containing all types of messages */

#include <stdlib.h>
#include <string.h>
#include "math.h"
#include "nmea_parser.h"

static const char str_gpgga[] = "GPGGA";
static const char str_gpgll[] = "GPGLL";
static const char str_gpgsa[] = "GPGSA";
static const char str_gpgsv[] = "GPGSV";
static const char str_gprmc[] = "GPRMC";
static const char str_gpvtg[] = "GPVTG";

/* Format strings for the different message types */
int fmt_gpgga[14] = {t_id, t_time, t_double, t_char, 	//time, lat, ns
		     t_double, t_char, t_int, 										//long, ew, pos_ind
		     t_int, t_double, t_double, 									//sats_used, hdop, msl
		     t_char, t_double, t_char, t_end}; 						//msl_unit, geoidal, geo_unit, end

int fmt_gpgll[9] = {t_id, t_double, t_char,
		    t_double, t_char, t_time,
		    t_char, t_char, t_end};

int fmt_gpgsa[8] = {t_id, t_char, t_int,
		    t_int12, t_double, t_double,
		    t_double, t_end};

int fmt_gpgsv[9] = {t_id, t_int, t_int,
		    t_int, t_int4, t_int4,
		    t_int4, t_int4, t_end};

int fmt_gprmc[14] = {t_id, t_time, t_char,
		     t_double, t_char, t_double,
		     t_char, t_double, t_double,
		     t_date, t_int, t_int, t_char, t_end};

int fmt_gpvtg[11] = {t_id, t_double, t_char,
		     t_double, t_char, t_double,
		     t_char, t_double, t_char,
		     t_char, t_end};


/* Set pointers to parse functions for the different types of fields */
int (*parse_func[NUM_TYPES]) (char **msg, void *result) = {parse_id, parse_char,
							   parse_double, parse_int,
							   parse_time, parse_date,
							   parse_int4, parse_int12};



/* Set sizes for the different types 
const int sizes[NUM_TYPES] = {(int)sizeof(MSG_ID), 
			      (int)sizeof(char), 
			      (int)sizeof(double), 
			      (int)sizeof(int),
			      (int)sizeof(struct Time), 
			      (int)sizeof(struct Date),
			      (int)sizeof(int)*4,
			      (int)sizeof(int)*12};*/
const int sizes[NUM_TYPES] = {1, 
															1, 
															8, 
															4,
															16, 
															12,
															16,
															48};

															/*
const int alignment[NUM_TYPES] = {0, //always first in message
																	(int)sizeof(char), 
																	(int)sizeof(double), 
																	(int)sizeof(int),
			      											(int)sizeof(int), //int first in time 
			      											(int)sizeof(int), //int first in date
			      											(int)sizeof(int),	//ints
			      											(int)sizeof(int)}; //ints*/
const int alignment[NUM_TYPES] = {0, //always first in message
																	1, 
																	8, 
																	4,
			      											8, //align on double 
			      											4, //int first in date
			      											4,	//ints
			      											4};

/* Set format strings for the message types */
int *fmts[NUM_MSGS] = {fmt_gpgga, fmt_gpgll, fmt_gpgsa, fmt_gpgsv, fmt_gprmc, fmt_gpvtg};


/* Verify checksum of NMEA message s
PARAM s: An NMEA message as a null terminated string
RETURN: 0 if checksum is correct otherwise 1
*/
int checksum(char *s) {
	int chksum = 0;
	int c = 0;
  if(*s != '$') {
    return -1;
  } else {
    s++;
  }
  while(*s != '*') {
    c ^= *s++;
  }
  s++;
	chksum = (int)strtol(s, NULL, 16);
  return (chksum == c ? 0 : 1);
}

/* Convert string representation of message type into MSG_ID of message type */
MSG_ID strToMsgID(char *msg) {
  if(strncmp(msg, str_gpgga, 5) == 0) {
    return GPGGA;
  } else if(strncmp(msg, str_gpgll, 5) == 0) {
    return GPGLL;
  } else if(strncmp(msg, str_gpgsa, 5) == 0) {
    return GPGSA;
  } else if(strncmp(msg, str_gpgsv, 5) == 0) {
    return GPGSV;
  } else if(strncmp(msg, str_gprmc, 5) == 0) {
    return GPRMC;
  } else if(strncmp(msg, str_gpvtg, 5) == 0) {
    return GPVTG;
  } else {
    return NMEA_ERROR;
  }
}

/*
Parse type of message
PARAM msg: pointer to first char of  Message ID in NMEA string
RETURN: type of message or NMEA_ERROR
*/
MSG_ID parse_msgType(char *msg, char **end_ptr) {
  MSG_ID type;
  char *end = *end_ptr;
  type = strToMsgID(msg);
  if(type != NMEA_ERROR) {
    end += 5; //advance to one past next comma
  }
  return type;
}

/* Parse message type */
int parse_id(char **msg, void *result) {
  MSG_ID *ret = (MSG_ID*)result;
  MSG_ID temp = NMEA_ERROR;
  char buf[6];
  int i = 0;
  for(i = 0; (**msg >= 'A' && **msg <= 'Z'); i++) {
    if(i <= 4) { 
      buf[i] = **msg;
      *msg += 1;
    } else {
      return 1;
    }
  }
  buf[5] = '\0';
  if(strcmp(buf, str_gpgga) == 0) {
    temp = GPGGA;
  } else if(strcmp(buf, str_gpgll) == 0) {
    temp = GPGLL;
  } else if(strcmp(buf, str_gpgsa) == 0) {
    temp = GPGSA;
  } else if(strcmp(buf, str_gpgsv) == 0) {
    temp = GPGSV;
  } else if(strcmp(buf, str_gprmc) == 0) {
    temp = GPRMC;
  } else if(strcmp(buf, str_gpvtg) == 0) {
    temp = GPVTG;
  } else {
    temp = NMEA_ERROR;
  }

  if(temp == NMEA_ERROR) {
    return 1;
  }
  *ret = temp;
  return 0;  
}

/* Parse an NMEA message of type mtype
   PARAM msg: Pointer to first char of message string, '$'
   PARAM ret: Pointer to struct for parsed values corresponding to message type mtype
   PARAM mtype: Type of message
   RETURN: 0 if successful otherwise 1
*/
int parse(char *msg, void *ret, MSG_ID mtype) {
  char *ptr = ret;
	unsigned int mask = 0;
	int power = 1;
  int i = 0;
  if(msg == NULL || *msg != '$' || checksum(msg)) {
    return 1;
  }
  //fmts[] holds format string
  //parse_func[] holds pointer to parse functions
  //sizes[] holds size of types in bytes (chars)
  //Loop through format array for message type
  for(i = 0; fmts[mtype][i] != t_end; i++) {
    msg++; //Skip $ and ,
		
    //parse field according to parse function set for that type
    if((*parse_func[fmts[mtype][i]])(&msg, (void*)ptr)) {
      return 1;
    }
    //Advance pointer in return struct
    ptr += sizes[fmts[mtype][i]];
		
		//Align memory address along boundaries, 4 for in, 8 for double etc.
		//Time and date structs align according to largest member (time: 8, date: 4)
		//Alignments stored in alignments[] and it would be a tiny bit prettier with a log2() func.
		//1. Don't need to align chars or end of string
		//2. Get a bitmask for the bits smaller than alignment
		//3. Null those bits if set and add alignment size
		
		/* ex. char stored at 0x17D8 (0001 0111 1101 1000)
			 ptr advanced to 0x17D9 (0001 0111 1101 1001) but next field is a double which need to be aligned on 8 byte.
			 Set mask to the 3 least significant bits (...001)
			 Mask is 1 so we enter the if-clause
			 Null the three least significant bits of 0x17D9 so it becomes 0x17D8, then add 8 and we get 0x17E0
			 which is aligned on 8 bytes. */
			 
		if(fmts[mtype][i+1] != t_end && fmts[mtype][i+1] != t_char) {
			mask = (int)ptr & ((int)(alignment[fmts[mtype][i+1]])-1); //Get bits from address, 3 least significant bits for 8byte alignment for example
			if(mask) { //If not aligned
				for(power = 1; pow(2,power) != alignment[fmts[mtype][i+1]]; power++); //no log2(), ugly loop
				ptr = (char*)(((unsigned int)ptr & (0xFFFFFFFF << power)) + alignment[fmts[mtype][i+1]]); //Null least significant bits and add alignment
			}
		}
		
  }
  
  return 0;

}

/* parse a double and save end point (next comma)
PARAM msg: ptr to ptr to first char in field to parse
PARAM result: ptr to space to save result
RETURN: 0 if successful, with result in result. Otherwise 1
*/
int parse_double(char **msg, void *result) {
  char *end_ptr;
  double parsed = 0;
  double *temp = (double*)result;
  //If no data in field:
  // - set result to -1
  // - return success
  if(**msg == ',') {
    *temp = -1;
    return 0;
  }

  if(!(**msg >= '0' && **msg <= '9') && **msg != '.') {
    *temp = -1;
    return 1;
  }

  parsed = strtod(*msg, &end_ptr);
  while((**msg >= '0' && **msg <= '9') || **msg == '.') {
    *msg += 1;
  }

  if(*msg != end_ptr) {
    return 1;
  }
  *msg = end_ptr;
  *temp = parsed;
  return 0;
}

/* parses two chars from msg as a number */
int parse_two(char *msg) {
  char buf[3] = "00";
  char *end = NULL;
  long int parsed = 0;
  strncpy(buf, msg, 2);
  parsed = strtol(buf, &end, 10);
  if(end != &buf[2]) {
    return -1;
  }

  return (int)parsed;
}


/* Parse time
PARAM msg: pointer to first char of time field in message
PARAM ret: pointer to a Time struct to store parsed values in
RETURN: 0 if successful otherwise 1
*/
int parse_time(char **msg, void *result) {
  int hours = 0;
  int minutes = 0;
  double seconds = 0.0;
  struct Time *time = (struct Time*)result;
  if(!(**msg >= '0' && **msg <= '9')) {
    time->hours = -1;
    time->minutes = -1;
    time->seconds = -1;
    return 0;
  }


  //hours
  hours = parse_two(*msg);
  if(hours == -1) {
    return 1;
  }
  
  *msg += 2;
  
  //minutes
  minutes = parse_two(*msg);
  if(minutes == -1) {
    return 1;
  }

  *msg += 2;

  //seconds
  if(parse_double(msg, &seconds)) {
    return 1;
  }
  time->hours = (short)hours;
  time->minutes = (short)minutes;
  time->seconds = seconds;
  
  //parse_double points end to the next comma, ready to return
  return 0;
}  

/* Parse int and save endpoint (next comma)
PARAM msg: ptr to ptr to first char in field to parse
PARAM result: pointer to int where result is saved
RETURNS: 1 if unsuccessful otherwise 0 with result in result
*/
int parse_int(char **msg, void *result) {
  char *end_ptr;
  int parsed = 0;
  int *temp = (int*)result;
  //If no data in field:
  // - set result to -1
  // - return success
  if(**msg == ',') {
    *temp = -1;
    return 0;
  }

  if(!(**msg >= '0' && **msg <= '9')) {
    *temp = -1;
    return 1;
  }

  parsed = strtol(*msg, &end_ptr, 10);
  while(**msg >= '0' && **msg <= '9') {
    *msg += 1;
  }

  if(*msg != end_ptr) {
    return 1;
  }
  *msg = end_ptr;
  *temp = parsed;
  return 0;
}
/* Parse a date field */
int parse_date(char **msg, void *result) {
  int day = 0;
  int month = 0;
  int year = 0;
  struct Date *date = result;
  
  if(**msg == ',') {
    date->day = -1;
    date->month = -1;
    date->year = -1;
    return 0;
  }

  //day
  day = parse_two(*msg);
  if(day == -1) {
    return -1;
  }

  *msg += 2;
  
  //month
  month = parse_two(*msg);
  if(month == -1) {
    return -1;
  }

  *msg += 2;
  
  if(parse_int(msg, &year)) {
    return -1;
  }
  
  date->year = year;
  date->month = month;
  date->day = day;
  return 0;
}

/* Parse a char from msg */  
int parse_char(char **msg, void *result) {
  char *ret = (char*)result;
  if(**msg == ',' || **msg == '*' || **msg == '\0') {
    *ret = 0; //result to (null)
    return 0;
  }

  *ret = **msg;
  *msg += 1;
  return 0;
}
/* Parse 12 ints in a row */
int parse_int12(char **msg, void *result) {
  int i = 0;
  int *ret = (int*)result;
  for(i = 0; i < 12; i++) {
    if(parse_int(msg, &ret[i])) {
	return 1;
    }
    *msg += 1;
  }
  *msg -= 1;
  return 0;
}

/* Parse 4 int in a row */
int parse_int4(char **msg, void *result) {
  int i = 0;
  int *ret = (int*)result;
  for(i = 0; i < 4; i++) {
    if(parse_int(msg, &ret[i])) {
	return 1;
    }
    *msg += 1;
  }
  *msg -= 1;
  return 0;
}

/* Parse a message of any type and save result into sub-struct of ret */
MSG_ID parser(char *msg, struct messages *ret) {
  char *msg_ptr = msg;
  char *next_ptr;
  MSG_ID type;
  int retval = 0;
  if(checksum(msg)) {
    return NMEA_ERROR;
  }
  msg_ptr++; //skip dollar sign
  type = parse_msgType(msg_ptr, &next_ptr);
  msg_ptr = msg; //Reset pointer to dollar sign
  if(type == NMEA_ERROR) { return NMEA_ERROR; }
	
  switch(type) {
  case GPGGA:
    retval = parse(msg_ptr, (void*)&ret->gpgga, type);
    break;
  case GPGLL:
    retval = parse(msg_ptr, (void*)&ret->gpgll, type);
    break;
  case GPGSA:
    retval = parse(msg_ptr, (void*)&ret->gpgsa, type);
    break;
  case GPGSV:
    retval = parse(msg_ptr, (void*)&ret->gpgsv, type);
    break;
  case GPRMC:
    retval = parse(msg_ptr, (void*)&ret->gprmc, type);
    break;
  case GPVTG:
    retval = parse(msg_ptr, (void*)&ret->gpvtg, type);
    break;
  default:
    return NMEA_ERROR;
  }
  //check retval
  if(retval) {
    return NMEA_ERROR;
  } else {
    return type;
  }
}
