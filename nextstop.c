/* 
 * Next stop module.
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "nextstop.h"
#include "global.h"
#include "database.h"
#include "task.h"


// Global scope pointer to the next stop.
Stop* nextStop;
xSemaphoreHandle nextStopLock;
//xQueueHandle skipStopQueue;


// The task which predicts next stop and updates nextStop pointer 
// respectively.
void nextStopTask(void* params) {

	int near = -1;
	int nextStopIndex = 0;
	
//	xQueueHandle skipStopQueue; 
	
	// Initialize next stop semaphore.
	vSemaphoreCreateBinary(nextStopLock);
	
	while (1)
	{
		int s;
		static int i = 1;
		// Get local copies
		Line* line = currentLine;
		int routeNum = currentRoute;
		Position current = getCurrentPosition();

		
//		skipStopQueue = (xQueueHandle *)params;
		// Test if we are near to some stop
		for (s = 0; s < line->routeLength[routeNum]; s++)
		{
			// Calculate distance to stop s.
			float distToStop = distanceInMeters(&current, &(line->routes[routeNum][s].position));
			
			// If got near to the stop
			if (near < 0 && distToStop < NEAR_THRESHOLD)
			{
				near = s;
			}
			// If got far from the stop s, it was passed.
			else if (near == s && distToStop >= NEAR_THRESHOLD)
			{
				nextStopIndex = near + 1;
				near = -1;
			}
		}
		
		// Check if the next stop was probably skipped.
		if (nextStopIndex < line->routeLength[routeNum] &&
		    distanceInMeters(&current, &(line->routes[routeNum][nextStopIndex+1].position))*2 
		    < distanceInMeters(&current, &(line->routes[routeNum][nextStopIndex].position)))
		{
			printf("Skipped %s!", line->routes[routeNum][nextStopIndex].name);
			nextStopIndex++;
			xQueueSend(skipStopQueue, &i, (portTickType) 10);
		}
		
		// If reached the end of route, change direction.
		if (near >= line->routeLength[routeNum]-1)
		{
			// Toggle route number
			if (routeNum == 1)
			{
				routeNum = 0;
			}
			else if (routeNum == 0)
			{
				routeNum = 1;
			}
			// Reset stop index.
			nextStopIndex = 0;
			near = -1;			
		}
		
		// Save predicted next stop to global scope.
		currentRoute = routeNum;
		
		if ( xSemaphoreTake(nextStopLock, portMAX_DELAY) == pdTRUE )
		{
			nextStop = &(line->routes[routeNum][nextStopIndex]);
			xSemaphoreGive(nextStopLock);
		}
		
		// Wait for 200 ms before recalculation.
		vTaskDelay(200 / portTICK_RATE_MS);
	}
}	


// Gets next stop pointer.
Stop* getNextStop(void)
{
	Stop* n = NULL;
	if ( xSemaphoreTake(nextStopLock, portMAX_DELAY) == pdTRUE )
	{
		n = nextStop;
		xSemaphoreGive(nextStopLock);
	}
	return n;
}


// Computes the distance, in meters, between two WGS-84 positions.
// By Julien Cayzac, October 02, 2008
// http://blog.julien.cayzac.name/2008/10/arc-and-distance-between-two-points-on.html
double distanceInMeters(Position* from, Position* to) {
    return EARTH_RADIUS_IN_METERS*arcInRadians(from, to);
}

// Computes the arc, in radian, between two WGS-84 positions.
// By Julien Cayzac, October 02, 2008
// http://blog.julien.cayzac.name/2008/10/arc-and-distance-between-two-points-on.html
double arcInRadians(Position* from, Position* to) {
    double latitudeArc  = (from->lat - to->lat) * DEG_TO_RAD;
    double longitudeArc = (from->lon - to->lon) * DEG_TO_RAD;
    double latitudeH = sin(latitudeArc * 0.5);
    double lontitudeH = sin(longitudeArc * 0.5);
    double tmp = cos(from->lat*DEG_TO_RAD) * cos(to->lat*DEG_TO_RAD);
    latitudeH *= latitudeH;
    lontitudeH *= lontitudeH;
    return 2.0 * asin(sqrt(latitudeH + tmp*lontitudeH));
}
