#ifndef __location_types_h__
#define __location_types_h__
#include "nmea_parser.h"

struct gps_position {
	double latitude;				
	double longitude;
	struct Time time;				//Time of fix
	int position_indicator; //0: not available or invalid, 1-2 or 6: valid
	int satellites_used;		//Number of satellites used in calculation
};



#endif
