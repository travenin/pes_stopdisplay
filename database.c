/*
 * Database module.
 *
 */
 
#include <stdlib.h>
#include <string.h>

#include "database.h"

// Array of all lines
Line* allLines[NUMBER_OF_LINES];

// Pointer to current line which was chosen to be driven.
Line* currentLine;

// Index of current route which is driven on the line.
// This is modified by Next stop module.
int currentRoute;

static Stop route0[4];
static Stop route1[4];
static Line testline;

static Stop route2[4];
static Stop route3[4];
static Line testline2;

// Build the bus line & stop database.
// TODO: Read files from SD card and build respectively.
void buildDatabase(void)
{
	int i;
	
	// Test line from Polacksbacken to Akademiska S�dra	
	allLines[0] = &testline;
	strcpy(testline.name, "77");

	i = 0;
	strcpy(route0[i].name, "Polacksbacken");
	route0[i].position.lat = 59.840239;
	route0[i].position.lon = 17.647725;
	i++;
	strcpy(route0[i].name, "Grindstugan");
	route0[i].position.lat = 59.840027;
	route0[i].position.lon = 17.639944;
	i++;
	strcpy(route0[i].name, "Science Park");
	route0[i].position.lat = 59.843112;
	route0[i].position.lon = 17.638742;
	i++;
	strcpy(route0[i].name, "Akademiska S.");
	route0[i].position.lat = 59.846845;
	route0[i].position.lon = 17.637385;
	
	testline.routes[0] = route0;
	testline.routeLength[0] = 4;
	strcpy(testline.routeName[0], "to Akademiska s.");

	
	i = 0;
	strcpy(route1[i].name, "Akademiska S.");
	route1[i].position.lat = 59.847132;
	route1[i].position.lon = 17.636959;
	i++;
	strcpy(route1[i].name, "Science Park");
	route1[i].position.lat = 59.843186;
	route1[i].position.lon = 17.638426;
	i++;
	strcpy(route1[i].name, "Grindstugan");
	route1[i].position.lat = 59.839976;
	route1[i].position.lon = 17.639585;
	i++;
	strcpy(route1[i].name, "Polacksbacken");
	route1[i].position.lat = 59.839941;
	route1[i].position.lon = 17.647704;
	
	testline.routes[1] = route1;
	testline.routeLength[1] = 4;
	strcpy(testline.routeName[1], "to Polacksbacken");
	


	// Test line around Polacksbacken.
	allLines[1] = &testline2;
	strcpy(testline2.name, "99");
	
	i = 0;
	strcpy(route2[i].name, "Hus 1 north door");
	route2[i].position.lat = 59.841532;
	route2[i].position.lon = 17.648189;
	i++;
	strcpy(route2[i].name, "Hus 4 front door");
	route2[i].position.lat = 59.840853;
	route2[i].position.lon = 17.648508;
	i++;
	strcpy(route2[i].name, "Rullan");
	route2[i].position.lat =  59.841033;
	route2[i].position.lon = 17.649925;
	i++;
	strcpy(route2[i].name, "Hus 3 east door");
	route2[i].position.lat = 59.84027;
	route2[i].position.lon = 17.650032;

	testline2.routes[0] = route2;
	strcpy(testline2.routeName[0], "To south");
	testline2.routeLength[0] = 4;
	
	i = 0;
	strcpy(route3[i].name, "Hus 3 east door");
	route3[i].position.lat = 59.84027;
	route3[i].position.lon = 17.650032;
	i++;
	strcpy(route3[i].name, "Rullan");
	route3[i].position.lat =  59.841033;
	route3[i].position.lon = 17.649925;
	i++;
	strcpy(route3[i].name, "Hus 4 front door");
	route3[i].position.lat = 59.840853;
	route3[i].position.lon = 17.648508;
	i++;
	strcpy(route3[i].name, "Hus 1 north door");
	route3[i].position.lat = 59.841532;
	route3[i].position.lon = 17.648189;
	
	testline2.routes[1] = route3;
	strcpy(testline2.routeName[1], "To north");
	testline2.routeLength[1] = 4;

		
	// Make global
	currentLine = allLines[0];
	currentRoute = 0;
}
