# GPS Based the Next Stop Display of a Bus #
This is the code repository of our project on Programming Embedded Systems course in Uppsala University, spring 2014.

The idea was to clone the *next stop* display which you might have seen in a bus. Such system is just displaying the name of the next bus stop on the route.

After starting the system the driver chooses the line and direction in the menu by using the physical buttons of the board. It the route's stops from the database, and changes it automatically according to the bus current GPS location. Instead of real big LED matrix this displays the it on the board's LCD. There are some extra information visible on the LCD too. Please see the demo video to see the idea in action.

## Demonstration video ##
http://youtu.be/ocvXbnrDn4Y

## Hardware ##
The program is developed to 

* *STM3210C-eval* evaluation board
* *GP-635T* GPS receiver module, which was connected to the board via USART.

## Software ##
The code uses *STM32 Standard Peripheral Library* for hardware interface and *FreeRTOS* V6.1.0 for thread handling.

## Developers ##
Mohit B. V., Johannes Gustavsson, Aditya Hendra, Lauri Virtanen

## Licence ##
This software is published under MIT free software license. See license.txt for more details.
