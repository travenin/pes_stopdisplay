
#ifndef GLOBAL_H
#define GLOBAL_H

#include "FreeRTOS.h"
#include "semphr.h"

// Stop is defined in database.
typedef struct Stop Stop;
extern xQueueHandle skipStopQueue;

// Position contains only latitude and longitude coordinates.
typedef struct {
	float lat;
	float lon;
} Position;


Position getCurrentPosition(void);
void setCurrentPosition(const Position* pos);

// Bus current position.
extern Position currentPosition;
extern xSemaphoreHandle currentPositionLock;


#endif
